﻿using System;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ExamWorkWPF
{
    public class FileService: IDownloader
    {
        public bool CheckFilePath(string filePath)
        {
            return (filePath != "" && Regex.IsMatch(filePath, @"^(?:[\a-zA-Z]\:|\\)(\\[a-zA-Z_\-\s0-9\.]+)+\.([A-Za-z0-9]+)$")) ? true : false;
        }

        private bool HasCreatedDirectory(string filePath)
        {
            string path = filePath.Substring(0, filePath.LastIndexOf(@"\"));

            try
            {
                var directory = new DirectoryInfo(path);
                if (!directory.Exists)
                {
                    directory.Create();
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        public async Task<string> Download(string url, string filePath)
        {
            if (System.IO.File.Exists(filePath))
            {
                return "File already exists!";
            }
            else if (!HasCreatedDirectory(filePath))
            {
                return "Try to choose another path for saving!";
            }

            try
            {
                await new WebClient().DownloadFileTaskAsync(new Uri(url), filePath);
            }
            catch (UriFormatException)
            {
                return "Check your url please!";
            }
            catch (WebException)
            {
                return "File couldn't be downloaded!";
            }
            catch
            {
                return "Unpredictable error!";
            }

            return "File was successfully downloaded!";
        }
    }
}
