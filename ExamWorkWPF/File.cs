﻿using System;

namespace ExamWorkWPF
{
    public class File
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public string Name { get; set; }

        public string Path { get; set; }
    }
}
