﻿namespace ExamWorkWPF
{
    using System.Data.Entity;

    public class DataContext : DbContext
    {
        public DataContext()
            : base("name=DataContext")
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<DataContext>());
        }

        public virtual DbSet<File> Files { get; set; }
    }
}